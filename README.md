# Outlook VBA DocuSign Email Attachment Processor

This script retrieves emails from a specified inbox subfolder with a .PDF attachment whose filename has 4 or 5 underscore separated substrings, saves the attachment with a suffix appended filename to a specified folder, and sends the attachment to a specified recipient in a new email.
